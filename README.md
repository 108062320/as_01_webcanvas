# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

    Thickness代表所有線條的粗度，包括Text、Eraser、Pen、Shape。
    Thickness是input type="number"的形式，可以直接在框框中輸入數字，也可以按右側的上下按鈕進行調整。
    
    Color代表所有繪圖工具的顏色，包括Text、Pen、Shape。
    Color是input type="color"的形式，可以直接選取調色盤中的顏色，也可以輸入RGB數值來取色。
    
    Text Font代表字型，用來更改Text的樣子。
    Text Font是一個選單，裡面有一些字型可以用滑鼠來選擇。
    
    畫面中的11個按鈕是作業的基本功能，皆可以正常使用。
    唯一需要留意的是Text判定使用者結束輸入的方式不是透過按下鍵盤的Enter，而是以滑鼠在任意地方點擊一下來判定。

### Function description

    畫面中的11個按鈕是作業的基本功能，皆可以正常使用。
    唯一需要留意的是Text判定使用者結束輸入的方式不是透過按下鍵盤的Enter，而是以滑鼠在任意地方點擊一下來判定。

### Gitlab page link

    https://108062320.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    :)

<style>
table th{
    width: 100%;
}
</style>
