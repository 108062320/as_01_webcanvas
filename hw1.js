
var mouse = {
    x: undefined,
    y: undefined,
    startX: undefined,
    startY: undefined,
    isDrawing: false,
    isTexting: false,
    startImage: undefined,
    mode: "Pen"
};


const canvas_width = 400;
const canvas_height = 400;
const canvas_top = "50px";
const canvas_left = "50px";
const canvas_position = "absolute";
const canvas_background_color = "white";

const Canvas = document.getElementById("myCanvas");
const Canvas_ctx = Canvas.getContext("2d");
Canvas_ctx.fillStyle = canvas_background_color;
Canvas_ctx.fillRect(0, 0, canvas_width, canvas_height);
Canvas_ctx.fillStyle = "black";

var memory = {
    array: [],
    index: -1
}


function drawLine(context, x1, y1, x2, y2) {
    context.lineJoin = "round";
    context.lineCap = "round";
    context.globalCompositeOperation = "source-over";
    context.beginPath();
    context.moveTo(x1, y1);
    context.lineTo(x2, y2);
    context.stroke();
    context.closePath();
};

function drawRectangle(context, x1, y1, x2, y2) {
    context.globalCompositeOperation = "source-over";

    context.putImageData(mouse.startImage, 0, 0);

    var rectW = x2 - x1;
    var rectH = y2 - y1;

    context.strokeRect(x1, y1, rectW, rectH);
};

function drawTriangle(context, x1, y1, x2, y2) {
    context.globalCompositeOperation = "source-over";

    context.putImageData(mouse.startImage, 0, 0);

    context.beginPath();
    context.moveTo(x1, y1);
    context.lineTo(x2, y2);
    context.lineTo(2 * x1 - x2, y2);
    context.closePath();
    context.stroke();
};

function drawCircle(context, x1, y1, x2, y2) {
    context.globalCompositeOperation = "source-over";

    context.putImageData(mouse.startImage, 0, 0);

    var centerX = (x1 + x2) / 2;
    var centerY = (y1 + y2) / 2;
    var radius = Math.abs((x1 - x2) / 2);
    context.beginPath();
    context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
    context.stroke();

};


function eraseLine(context, x1, y1, x2, y2) {
    context.lineJoin = "round";
    context.lineCap = "round";
    context.globalCompositeOperation = 'destination-out';
    context.beginPath();
    context.moveTo(x1, y1);
    context.lineTo(x2, y2);
    context.stroke();
    context.closePath();
}

function tool_clear() {
    Canvas_ctx.clearRect(0, 0, canvas_width, canvas_height);
    memory.array.length = 0;
    memory.index = -1;
};

function tool_redo() {
    if (memory.index + 1 < memory.array.length) {
        memory.index += 1;
        Canvas_ctx.putImageData(memory.array[memory.index], 0, 0);
    }
}

function tool_undo() {
    if (memory.index > 0) {
        memory.index -= 1;
        Canvas_ctx.putImageData(memory.array[memory.index], 0, 0);
    }
}

function tool_erase() {
    mouse.mode = "Eraser"
}

function tool_pen() {
    mouse.mode = "Pen";
}

function tool_rectangle() {
    mouse.mode = "Rectangle";
}

function tool_triangle() {
    mouse.mode = "Triangle";
}

function tool_circle() {
    mouse.mode = "Circle";
}

function tool_text() {
    mouse.mode = "Text";
}

function tool_download() {
    var mycanvas = Canvas.toDataURL("image/jpeg");
    console.log(Canvas);
    var a = document.getElementById("download_a");
    a.href = mycanvas;
}

function changeThickness() {
    let thickness = document.getElementById("thickness").value;
    let selector = document.getElementById("font_selector");
    let color = document.getElementById("color").value;
    Canvas_ctx.strokeStyle = color;
    Canvas_ctx.fillStyle = color;
    Canvas_ctx.lineWidth = thickness;
    curfont = thickness + "px " + selector.value;
}

function changeColor() {
    let thickness = document.getElementById("thickness").value;
    let selector = document.getElementById("font_selector");
    let color = document.getElementById("color").value;
    Canvas_ctx.strokeStyle = color;
    Canvas_ctx.fillStyle = color;
    Canvas_ctx.lineWidth = thickness;
    curfont = thickness + "px " + selector.value;
}

var curfont = "5px" + " " + "Arial";
function font_select(){
    let selector = document.getElementById("font_selector");
    let thickness = document.getElementById("thickness").value;
    let color = document.getElementById("color").value;
    Canvas_ctx.strokeStyle = color;
    Canvas_ctx.fillStyle = color;
    Canvas_ctx.lineWidth = thickness;
    curfont = thickness + "px " + selector.value;
}

var text_blank = document.createElement("input");
var body = document.body;
body.appendChild(text_blank);
function init_text_bar() {
    text_blank.type = "text";
    text_blank.id = "text_blank";
    text_blank.style.position = "absolute";
    // text_blank.style.width = "120px";
    // text_blank.style.height = "24px";
    text_blank.width = "120px";
    text_blank.height = "24px";
    text_blank.style.backgroundColor = "white";
    text_blank.style.border = "2px solid black";
    text_blank.style.top = "800px"
    text_blank.style.left = "0px";
    text_blank.value = "";
    text_blank.hidden = true;
}
init_text_bar();

var upload_image = document.getElementById("upload_img");
upload_image.hidden = true;

function tool_upload() {
    upload_image.click();
}

upload_image.onchange = function (event) {
    var image = new Image();
    image.onload = function () {
        var w = this.width;
        var h = this.height;
        Canvas_ctx.globalCompositeOperation = "source-over";
        Canvas.width = w;
        Canvas.height = h;
        Canvas_ctx.drawImage(this, 0, 0, w, h);
        memory.array.push(Canvas_ctx.getImageData(0, 0, Canvas.width, Canvas.height));
    }
    image.src = URL.createObjectURL(event.target.files[0]);

    memory.array.length = memory.index + 1;
    memory.index += 1;
}

Canvas.addEventListener("mousedown",
    function (event) {
        mouse.x = event.offsetX;
        mouse.y = event.offsetY;
        if (mouse.mode == "Text") {
            if (mouse.isTexting == false) {
                text_blank.style.top = (event.pageY - 12) + "px";
                text_blank.style.left = event.pageX + "px";
                text_blank.hidden = false;
                mouse.isTexting = true;
                mouse.startX = event.offsetX;
                mouse.startY = event.offsetY;
                // console.log(text_blank.style.left);
                // console.log(text_blank.style.top);
            }
            // else{
            //     mouse.isTexting = false;
            //     console.log(text_blank.style.left);
            //     console.log(text_blank.style.top);
            // }
        }
        else if (mouse.mode == "Rectangle") {
            mouse.startImage = Canvas_ctx.getImageData(0, 0, canvas_width, canvas_height);
            mouse.isDrawing = true;
            mouse.startX = event.offsetX;
            mouse.startY = event.offsetY;
        }
        else if (mouse.mode == "Triangle") {
            mouse.startImage = Canvas_ctx.getImageData(0, 0, canvas_width, canvas_height);
            mouse.isDrawing = true;
            mouse.startX = event.offsetX;
            mouse.startY = event.offsetY;
        }
        else if (mouse.mode == "Circle") {
            mouse.startImage = Canvas_ctx.getImageData(0, 0, canvas_width, canvas_height);
            mouse.isDrawing = true;
            mouse.startX = event.offsetX;
            mouse.startY = event.offsetY;
        }
        else {
            mouse.isDrawing = true;
            mouse.startX = event.offsetX;
            mouse.startY = event.offsetY;
        }
    }
);

text_blank.addEventListener("mouseup",
    function (event) {
        if (mouse.mode == "Text") {
            text_blank.focus();
        }
    }
)

text_blank.addEventListener("focusout",
    function (event) {
        // Canvas_ctx.font = "24px Georgia";
        Canvas_ctx.globalCompositeOperation = "source-over";
        Canvas_ctx.font = curfont;
        if (text_blank.value != "") Canvas_ctx.fillText(text_blank.value, mouse.startX, mouse.startY);
        console.log(Canvas_ctx);
        init_text_bar();
        mouse.isTexting = false;

        memory.array.length = memory.index + 1;
        memory.array.push(Canvas_ctx.getImageData(0, 0, canvas_width, canvas_height));
        memory.index += 1;
    }
)

Canvas.addEventListener("mousemove",
    function (event) {
        if (mouse.mode == "Pen") {
            Canvas.style.cursor = "url('img/Pen.png') 0 24, auto";
        }
        else if (mouse.mode == "Rectangle") {
            Canvas.style.cursor = "url('img/Rectangle.png') 0 0, auto";
        }
        else if (mouse.mode == "Triangle") {
            Canvas.style.cursor = "url('img/Triangle.png') 0 0, auto";
        }
        else if (mouse.mode == "Circle") {
            Canvas.style.cursor = "url('img/Circle.png') 0 0, auto";
        }
        else if (mouse.mode == "Eraser") {
            Canvas.style.cursor = "url('img/Eraser.png') 0 24, auto";
        }
        else if (mouse.mode == "Text") {
            Canvas.style.cursor = "url('img/Text.png') 0 12, auto";
        }

        if (mouse.isDrawing === true) {
            if (mouse.mode == "Pen") drawLine(Canvas_ctx, mouse.x, mouse.y, event.offsetX, event.offsetY);
            else if (mouse.mode == "Eraser") eraseLine(Canvas_ctx, mouse.x, mouse.y, event.offsetX, event.offsetY);
            else if (mouse.mode == "Rectangle") drawRectangle(Canvas_ctx, mouse.startX, mouse.startY, event.offsetX, event.offsetY);
            else if (mouse.mode == "Triangle") drawTriangle(Canvas_ctx, mouse.startX, mouse.startY, event.offsetX, event.offsetY);
            else if (mouse.mode == "Circle") drawCircle(Canvas_ctx, mouse.startX, mouse.startY, event.offsetX, event.offsetY);
            mouse.x = event.offsetX;
            mouse.y = event.offsetY;
        }
    }
);

window.addEventListener("mouseup",
    function (event) {
        if (mouse.isDrawing === true) {
            if (mouse.mode == "Pen") drawLine(Canvas_ctx, mouse.x, mouse.y, event.offsetX, event.offsetY);
            else if (mouse.mode == "Eraser") eraseLine(Canvas_ctx, mouse.x, mouse.y, event.offsetX, event.offsetY);
            mouse.x = 0;
            mouse.y = 0;
            mouse.isDrawing = false;

            memory.array.length = memory.index + 1;
            memory.array.push(Canvas_ctx.getImageData(0, 0, canvas_width, canvas_height));
            memory.index += 1;
        }
    }
);

Canvas.addEventListener("mouseleave",
    function (event) {
        if (mouse.isDrawing === true) {
            if (mouse.mode == "Pen") drawLine(Canvas_ctx, mouse.x, mouse.y, event.offsetX, event.offsetY);
            else if (mouse.mode == "Eraser") eraseLine(Canvas_ctx, mouse.x, mouse.y, event.offsetX, event.offsetY);
            mouse.x = 0;
            mouse.y = 0;
            mouse.isDrawing = false;

            memory.array.length = memory.index + 1;
            memory.array.push(Canvas_ctx.getImageData(0, 0, canvas_width, canvas_height));
            memory.index += 1;
        }
    }
);




